# ubuntu-mate-welcome

The Ubuntu MATE Welcome screen. 

Based on:

  * https://github.com/Antergos/antergos-welcome
  * http://blog.schlomo.schapiro.org/2014/01/apt-install.html

## Requirements

  * gir1.2-gtk-3.0
  * gir1.2-notify-0.7
  * gir1.2-webkit-3.0
  * libgtk2-perl
  * policykit-1
  * python3
  * python3-apt
  * python3-aptdaemon
  * python3-aptdaemon.gtk3widgets
  * python3-gi
  * software-properties-common

The social icons were created by Paul Robert Lloyd and licenced
under a Creative Commons Attribution-Share Alike 3.0 Licence.

  * http://paulrobertlloyd.com/2009/06/social_media_icons/
